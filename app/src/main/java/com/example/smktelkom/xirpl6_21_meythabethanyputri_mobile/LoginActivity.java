package com.example.smktelkom.xirpl6_21_meythabethanyputri_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.smktelkom.xirpl6_21_meythabethanyputri_mobile.koneksi.VolleySingleton;
import com.example.smktelkom.xirpl6_21_meythabethanyputri_mobile.model.UserModel;
import com.example.smktelkom.xirpl6_21_meythabethanyputri_mobile.session.SessionPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    private final String url = "http://10.1.5.22:8080/users";
    EditText Username, Password;
    Button Login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (SessionPreference.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }

        Username = findViewById(R.id.username);
        Password = findViewById(R.id.password);
        Login = findViewById(R.id.login);

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin();
            }
        });

    }

    private void userLogin() {
        final String username = Username.getText().toString();
        final String password = Password.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String user = object.getString("UserName");
                        String pass = object.getString("Password");
                        Log.d("data", "onResponse:" + user + pass);
                        if (Username.equals(user) && Password.equals(pass)) {
                            UserModel userModel = new UserModel(
                                    object.getInt("IDUser"),
                                    object.getString("Name"),
                                    object.getString("UserName"),
                                    object.getString("Password"),
                                    object.getString("Branch")
                            );
                            SessionPreference.getInstance(getApplicationContext()).UserLogin(userModel);
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

}
