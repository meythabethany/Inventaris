package com.example.smktelkom.xirpl6_21_meythabethanyputri_mobile;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import com.example.smktelkom.xirpl6_21_meythabethanyputri_mobile.model.UserModel;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    public static final String TAG_CATEGORY = "Category";
    private static final String JSON_ARRAY = "result";
    private final String urlKategori = " http://192.168.0.109/rest_ukl/index.php/kategori";
    private final String urlJenis = " http://192.168.0.109/rest_ukl/index.php/jenis";
    private final String urlBarang = " http://192.168.0.109/rest_ukl/index.php/barang";
    private TextView nama;
    private TextView txtID, txtAset, txtSpec, txtTgl, txtRuang, txtCat, txtJenis, txtHarga, txtInv, txtSatuan;
    private EditText etAset, etSpec, etTgl, etRuang, etHarga, etSatuan;
    private TableLayout tabLayout;
    private Button logout, insert;
    private Spinner spKategori, spJenis;
    private ArrayList<String> kategori = new ArrayList<>();
    private ArrayList<String> elektronik = new ArrayList<>();
    private ArrayList<String> nonelektronik = new ArrayList<>();
    private Calendar myCalendar;
    private String idjenis;
    private DatePickerDialog.OnDateSetListener date;
    private JSONArray result;
    private UserModel user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
