package com.example.smktelkom.xirpl6_21_meythabethanyputri_mobile.model;

public class BarangModel {

    private int id, jenis, harga;
    private String invent;
    private String aset;
    private String spec;
    private String ruang;
    private String tgl;
    private String category;
    private String satuan;

    public BarangModel(int id, int jenis, int harga, String invent, String aset, String spec, String ruang, String tgl, String category, String satuan) {
        this.id = id;
        this.jenis = jenis;
        this.harga = harga;
        this.invent = invent;
        this.aset = aset;
        this.spec = spec;
        this.ruang = ruang;
        this.tgl = tgl;
        this.category = category;
        this.satuan = satuan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getJenis() {
        return jenis;
    }

    public void setJenis(int jenis) {
        this.jenis = jenis;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getInvent() {
        return invent;
    }

    public void setInvent(String invent) {
        this.invent = invent;
    }

    public String getAset() {
        return aset;
    }

    public void setAset(String aset) {
        this.aset = aset;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getRuang() {
        return ruang;
    }

    public void setRuang(String ruang) {
        this.ruang = ruang;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }
}
