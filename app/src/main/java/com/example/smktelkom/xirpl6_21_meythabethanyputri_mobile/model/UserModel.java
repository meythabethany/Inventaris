package com.example.smktelkom.xirpl6_21_meythabethanyputri_mobile.model;

public class UserModel {

    int id;
    String Name;
    String Username;
    String Password;
    String Branch;

    public UserModel(int id, String Name, String Username, String Password, String Branch) {
        this.id = id;
        this.Name = Name;
        this.Username = Username;
        this.Password = Password;
        this.Branch = Branch;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getBranch() {
        return Branch;
    }

    public void setBranch(String branch) {
        Branch = branch;
    }

}
